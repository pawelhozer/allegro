    Program wczytuje kolejne linie jedna po drugiej i następnie wypisuje je na ekran.
Po wypisaniu danej linii program oczekuje na wprowadzenie poprawnego tekstu zatwierdzonego
"enterem" jeżeli wprowadzony przez użytkownika tekst się zgadza program wypisuje kolejna linijkę
i czeka na tekst od użytkownika.W przypadku błędnego wprowadzenia tekstu program wypisze index pierwszego niepoprawnie
wprowadzonego znaku i odpowiedni komunikat, który pomoże użytkownikowi zlokalizować błąd.Po wprowadzeniu poprawnego tekstu
 program wypisze "Gratulacje".

 Krótki opis mojego kodu:

 WYMAGANA JAVA 11

1. W kodzie użyłem zewnętrznych bibliotek RichTextFX, JavaFX oraz JUnit4

2. Program uruchamiamy startując main z klasy FXAPPLauncher, by załączyć komponenty

3. MainViewController obsługuje dwa eventy, wybranie pliku z tekstem na wejściu z menu aplikacji
    oraz wprowadzenie tekstu za pomocą klawisza enter wraz z przewinięciem TextArea, gdy tekst nie mieści się w oknie,

4. Klasa OutputDataToTextArea to klasa odpowiedzialna za wypisywanie komunikatów na ekran

5. Klasa MarkString odpowiedzialna jest za odpowiednie zaznaczanie kolorem odpowiednich komunikatów na ekranie
   zielony - prawidłowo przepisany tekst, czerwony - błąd, niebieski - wiadomość od programu, czarny - default, do przepisania.
   Użyłem opcji dołączenia styli jako arkusza CSS, który znajduje się w resources/css

6. Klasa DataFromTxtFile to klasa odpowiedzialna za wczytywanie linii z pliku tekstowego i zapisywanie ich do kolejki FIFO.
   Klasa ta też waliduje wczytany plik i sprawdza czy nie jest on nullem bądź pusty. Z kolejki FIFO z tego pliku kolejno
   pobierane są wczytane linie, które następnie są porównywane w klasie LinesValidator.

7. LinesValidator to klasa odpowiedzialna za sprawdzenie czy linia z pliku tekstowego oraz pobrana z TextField
   są identyczne, jeśli nie są, klasa zwraca index pierwszego napotkanego błędu, jeśli są, klasa zwraca -1, co oznacza
   że linie są identyczne

8. Do klas, które przeprowadzają kluczowe operacje, to jest wczytanie danych z pliku(DataFromTxtFile.class) oraz
   walidacja czy są takie same(LinesValidator.class) napisałem testy jednostkowe, które znajdują się w folderze test/java
