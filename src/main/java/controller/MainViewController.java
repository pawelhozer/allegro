package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import org.fxmisc.richtext.StyleClassedTextArea;
import service.OutputDataToTextArea;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class MainViewController implements Initializable {

    @FXML
    private StyleClassedTextArea textArea;
    @FXML
    private TextField textField;
    @FXML
    private MenuItem menuItem;

    private OutputDataToTextArea outputDataToTextArea;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        textField.setEditable(false);
        textArea.setEditable(false);
        textArea.appendText("Program polega na przepisywaniu bezbłędnie kolejnych linii tekstu.\n" +
                "Jeśli popełnisz błąd, będziesz musiał przepisć tą samą linię jeszcze raz. \n");
        textArea.appendText("Wybierz plik źródłowy z tekstem: File -> Load new file\n" );
    }

    public void onSelectFile(ActionEvent actionEvent){
      menuItem.setOnAction(actionEvent1 -> {
          try {
              outputDataToTextArea = new OutputDataToTextArea(textField,textArea);
              outputDataToTextArea.readTextFile();
              outputDataToTextArea.readSingleLine();
              textField.setEditable(true);
          } catch (IOException e) {
              outputDataToTextArea = new OutputDataToTextArea(textField,textArea);
          }
      });
    }

    public void pressedEnter(ActionEvent actionEvent){
        textField.setOnKeyPressed(keyEvent -> {
            if(keyEvent.getCode().equals(KeyCode.ENTER) && textField.isEditable()){
                outputDataToTextArea.readLineFromTextField();
                outputDataToTextArea.searchDifference();
            }
        });
        textArea.textProperty().addListener(new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
                textArea.scrollYBy(Double.MAX_VALUE);
            }
        });
    }

}
