package service;

import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import org.fxmisc.richtext.StyleClassedTextArea;
import repository.DataFromTxtFile;

import java.io.File;
import java.io.IOException;


public class OutputDataToTextArea {

    private TextField textField;
    private StyleClassedTextArea textArea;

    private LinesValidator linesValidator;
    private DataFromTxtFile dataFromTxtFile;
    private MarkString markString;

    private String lineFromFile;
    private String lineFromTextField;


    public OutputDataToTextArea(TextField textField, StyleClassedTextArea textArea) {
        this.textField = textField;
        this.textArea = textArea;

        linesValidator = new LinesValidator();
        dataFromTxtFile = new DataFromTxtFile();
        markString = new MarkString(textArea);
    }

    public void readSingleLine(){
        lineFromFile = dataFromTxtFile.getLine();
        if(lineFromFile.isEmpty()){
            String greetings = ("Gratulacje, udało Ci się poprawnie wykonać całe zadanie! \nWczytaj nowy plik by spróbować ponownie: \n");
            textArea.appendText(greetings);
            markString.markAsConsoleInfo(greetings);
            textField.setEditable(false);
        }else{
            textArea.appendText(lineFromFile+"\n");
            markString.markAsDefault(lineFromFile);
        }
    }

    public void readTextFile() throws IOException {

        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(null);


        if(dataFromTxtFile.isFileNullOrEmpty(file)){
            String badFilePath = "Plik jest pusty lub nieprawidłowo wczytany\n";
            String loadNewFile = "Wybierz plik źródłowy z tekstem: File -> Load new file\n";

            textArea.appendText(badFilePath);
            markString.markAsConsoleInfo(badFilePath);

            textArea.appendText(loadNewFile);
            markString.markAsDefault(loadNewFile);

            throw new IOException("Nieprawidłowy plik");
        }else{
            String loadedFile = "Wyczytano plik o nazwie: " + file.getName() + "\n";

            textArea.appendText(loadedFile);
            markString.markAsConsoleInfo(loadedFile);

            dataFromTxtFile.readDataFromFile(file);
        }
    }

    public void readLineFromTextField(){
        lineFromTextField = textField.getText();
        textField.clear();
    }

    public void searchDifference(){

        if (isEmptyTextField()) return;

        int difference = linesValidator.findFirstIndexOfDifference(lineFromFile,lineFromTextField);

        if(difference!=-1){

            String mistake = "Błąd na pozycji: " + difference + " Twój błąd to: \n";

            textArea.appendText(mistake);
            markString.markAsConsoleInfo(mistake);

            textArea.appendText(lineFromTextField +"\n");
            markString.markAsError(lineFromTextField,difference);

            textArea.appendText("Spróbuj przepisać zdanie ponownie:\n" + lineFromFile +"\n");
            markString.markAsDefault(lineFromFile);
        }else{
            textArea.appendText(lineFromTextField + "\n");
            markString.markAsGood(lineFromTextField);
            readSingleLine();
        }
    }

    private boolean isEmptyTextField(){
        if(lineFromTextField.isEmpty()){

            String emptyString = "Podałeś pusty łańcuch znaków!\n";

            textArea.appendText(emptyString);
            markString.markAsConsoleInfo(emptyString);

            textArea.appendText(lineFromFile + "\n");
            markString.markAsDefault(lineFromFile);
            return true;
        }
        return false;
    }
}
