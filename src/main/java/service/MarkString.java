package service;

import org.fxmisc.richtext.StyleClassedTextArea;

import java.util.Collections;

class MarkString {

    private StyleClassedTextArea textArea;

    MarkString(StyleClassedTextArea textArea){
        this.textArea = textArea;
    }

    void markAsGood(String source){
        textArea.setStyle(textArea.getLength()-source.length()-1,textArea.getLength(), Collections.singleton("correct"));
    }

    void markAsDefault(String source){
        textArea.setStyle(textArea.getLength()-source.length()-1,textArea.getLength(), Collections.singleton("default"));
    }

    void markAsError(String source, int differencePosition){
        if(source.isEmpty())return;
        textArea.setStyle(textArea.getLength()-source.length()+differencePosition-1,textArea.getLength()-source.length()+differencePosition, Collections.singleton("error"));
    }

    void markAsConsoleInfo(String source){
        textArea.setStyle(textArea.getLength()-source.length()-1,textArea.getLength(), Collections.singleton("exception"));
    }

}
