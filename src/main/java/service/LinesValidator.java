package service;

class LinesValidator {

    int findFirstIndexOfDifference(String lineFromFile, String lineFromTextField) {

        if (lineFromFile.equals(lineFromTextField)) return -1;

        int differentIndex;
        for (differentIndex = 0; differentIndex < lineFromFile.length() && differentIndex < lineFromTextField.length(); ++differentIndex) {
            if (lineFromFile.charAt(differentIndex) != lineFromTextField.charAt(differentIndex)) {
                return differentIndex;
            }
        }
        if(differentIndex<lineFromFile.length() || differentIndex<lineFromTextField.length()){
            return differentIndex;
        }
        return -1;
    }

}
