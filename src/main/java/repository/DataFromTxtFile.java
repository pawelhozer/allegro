package repository;

import java.io.*;
import java.util.ArrayDeque;
import java.util.Queue;

public class DataFromTxtFile {

    private Queue<String> myLines;

    public DataFromTxtFile(){
        this.myLines = new ArrayDeque<>();
    }

    public void readDataFromFile(File file) throws IOException {

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                myLines.offer(line);
            }
        }
    }

    public String getLine() {
        if(myLines.size() == 0) return "";
        return myLines.poll();
    }

    public boolean isFileNullOrEmpty(File file){
        if(file == null || file.length()==0){
            return true;
        }
        return false;
    }
}
