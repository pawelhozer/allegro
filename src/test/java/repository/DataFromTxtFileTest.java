package repository;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.net.URL;
import java.util.ArrayDeque;
import java.util.Queue;

import static org.junit.Assert.*;

public class DataFromTxtFileTest {

    private DataFromTxtFile dataFromTxtFile;
    private ClassLoader classLoader = getClass().getClassLoader();
    private File file = new File(classLoader.getResource("example.txt").getFile());

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Before
    public void setUp(){
        dataFromTxtFile = new DataFromTxtFile();
    }

    @Test
    public void isFileNull(){
        assertTrue(dataFromTxtFile.isFileNullOrEmpty(null));
    }

    @Test
    public void isFileEmpty() throws IOException {
        File file = temporaryFolder.newFile("testFile.txt");
        assertTrue(dataFromTxtFile.isFileNullOrEmpty(file));
    }

    @Test
    public void isCorrectFile(){
        assertFalse(dataFromTxtFile.isFileNullOrEmpty(file));
    }

    @Test
    public void readDataFromFileAndGetLine() throws IOException {
        int numberOfLines = 0;

        dataFromTxtFile.readDataFromFile(file);

        while(!dataFromTxtFile.getLine().isEmpty()){
            numberOfLines++;
        }
        assertEquals(5,numberOfLines);
    }
}