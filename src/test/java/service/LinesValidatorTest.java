package service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class LinesValidatorTest {

    private LinesValidator linesValidator;
    private int expectedResult;
    private String stringFromFile;
    private String stringFromInput;

    @Before
    public void setUp() throws Exception {
        linesValidator = new LinesValidator();
    }

    public LinesValidatorTest(String stringFromFile, String stringFromInput, int expectedResult) {
        this.stringFromFile = stringFromFile;
        this.stringFromInput = stringFromInput;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection randomStrings(){
        return Arrays.asList(new Object[][]{
                {"Text Simple","Simple text",0},
                {"","Simple text",0},
                {"Simple text","Simple text1",11},
                {"Simple text","Simple text",-1}
        });
    }

    @Test
    public void testFindFirstIndexOfDifference() {
        assertEquals(expectedResult,linesValidator.findFirstIndexOfDifference(stringFromFile,stringFromInput));
    }
}